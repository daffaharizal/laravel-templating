<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('casts.index', ['casts' => $casts]);
    }

    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        $cast = Cast::create($validatedData);

        return redirect('/cast')->with('success', 'Pemain film berhasil ditambahkan!');
    }

    public function show($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        return view('casts.show', ['cast' => $cast]);
    }

    public function edit($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        return view('casts.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $cast_id)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        $cast = Cast::findOrFail($cast_id);
        $cast->update($validatedData);

        return redirect('/cast')->with('success', 'Data pemain film berhasil diperbarui!');
    }

    public function destroy($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        $cast->delete();

        return redirect('/cast')->with('success', 'Data pemain film berhasil dihapus!');
    }
}
