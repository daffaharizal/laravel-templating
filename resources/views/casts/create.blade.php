@extends('layout.index')

@section('content')
<div class="container">
    <h2>Tambah Pemain Film Baru</h2>
    <form method="POST" action="{{ url('/cast') }}">
        @csrf
        <div class="form-group">
            <label for="nama">Nama:</label>
            <input type="text" class="form-control" id="nama" name="nama" required>
        </div>
        <div class="form-group">
            <label for="umur">Umur:</label>
            <input type="number" class="form-control" id="umur" name="umur" required>
        </div>
        <div class="form-group">
            <label for="bio">Bio:</label>
            <textarea class="form-control" id="bio" name="bio" rows="3" required></textarea>
        </div>
        <button type="submit" class="btn btn-success">Tambah Pemain Film</button>
        <a href="{{ url('/cast') }}" class="btn btn-secondary">Kembali</a>
    </form>
</div>
@endsection