@extends('layout.index')

@section('content')
<!-- <div class="container">
    <h2>Daftar Pemain Film</h2>
    <a href="{{ url('/cast/create') }}" class="btn btn-success mb-3">Tambah Pemain Film</a>

    <table class="table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($casts as $cast)
            <tr>
                <td>{{ $cast->nama }}</td>
                <td>{{ $cast->umur }}</td>
                <td>{{ $cast->bio }}</td>
                <td>
                    <a href="{{ url('/cast/'.$cast->id) }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="{{ url('/cast/'.$cast->id.'/edit') }}" class="btn btn-warning btn-sm">Edit</a>
                    <form action="{{ url('/cast/'.$cast->id) }}" method="POST" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus?')">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div> -->
<div class="container">
    <h2>Detail Pemain Film</h2>
    <table class="table">
        <tbody>
            <tr>
                <th>Nama</th>
                <td>{{ $cast->nama }}</td>
            </tr>
            <tr>
                <th>Umur</th>
                <td>{{ $cast->umur }}</td>
            </tr>
            <tr>
                <th>Bio</th>
                <td>{{ $cast->bio }}</td>
            </tr>
        </tbody>
    </table>
    <a href="{{ url('/cast') }}" class="btn btn-secondary">Kembali</a>
</div>
@endsection