@extends('layout.index')

@section('content')
<div class="container">
    <h2>Edit Pemain Film</h2>
    <form method="POST" action="{{ url('/cast/'.$cast->id) }}">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="nama">Nama:</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{ $cast->nama }}" required>
        </div>
        <div class="form-group">
            <label for="umur">Umur:</label>
            <input type="number" class="form-control" id="umur" name="umur" value="{{ $cast->umur }}" required>
        </div>
        <div class="form-group">
            <label for="bio">Bio:</label>
            <textarea class="form-control" id="bio" name="bio" rows="3" required>{{ $cast->bio }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        <a href="{{ url('/cast') }}" class="btn btn-secondary">Kembali</a>
    </form>
</div>
@endsection